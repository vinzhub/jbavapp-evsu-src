import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:jbavapp/page/newanalogpage.dart';
import 'package:jbavapp/page/newchargemodepage.dart';
import 'package:jbavapp/page/newsensorpage.dart';
import 'package:jbavapp/page/newtimerpage.dart';
import 'package:jbavapp/utils/appmastermgr.dart';
import 'package:jbavapp/utils/chargingmode.dart';
import 'package:shared_preferences/shared_preferences.dart';

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
class ProfileHomeWidget extends StatefulWidget {
  final String _key;
  ProfileHomeWidget(this._key);
  @override
  _ProfileHomeWidgetState createState() => _ProfileHomeWidgetState();
}

class _ProfileHomeWidgetState extends State<ProfileHomeWidget> {
  Timer refreshTimer;
  refresh() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    refreshTimer = Timer.periodic(Duration(seconds: 10), (refreshTimer) {
      setState(() {});
    });
    AppMasterManager.instance().listeners['relay'] = refresh;
  }

  @override
  void dispose() {
    super.dispose();
    refreshTimer.cancel();
    AppMasterManager.instance().listeners.remove('relay');
  }

  // TextStyle btnstyle1 = TextStyle(
  //   fontSize: 60,
  //   fontWeight: FontWeight.bold,
  //   fontFamily: "Helvetica",
  // );

  // TextStyle btnstyle2 = TextStyle(
  //   fontSize: 15,
  //   fontWeight: FontWeight.bold,
  // );

  MaterialColor getColor(relay) {
    var color;
    if (relay == '0') {
      color = Colors.red;
    } else {
      color = Colors.green;
    }
    return color;
  }

  Widget profileHome() {
    return Column(
      children: [
        profileRelay(
          'RELAY A',
          'r1',
          AppMasterManager.instance().getRelayState('A', widget._key),
          //AppManager.instance().relayAState(widget.profileName),
        ),
        profileRelay(
          'RELAY B',
          'r2',
          AppMasterManager.instance().getRelayState('B', widget._key),
        ),
      ],
    );
  }

  Widget profileRelay(name, relay, state) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: Container(
              alignment: Alignment.center,
              width: double.infinity,
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: getColor(state),
                  width: 30.0,
                ),
                borderRadius: new BorderRadius.all(
                  new Radius.circular(120.0),
                ),
              ),
              child: Text(
                name,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 55,
                  fontWeight: FontWeight.bold,
                  color: getColor(state),
                ),
                //style: AppMasterManager.instance().title1,
                // style: TextStyle(
                //   fontSize: 75.0,
                //   fontWeight: FontWeight.bold,
                // ),
              ),
            ),
          ),
          Flexible(
            fit: FlexFit.tight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: OutlineButton(
                    textColor: Colors.red,
                    onPressed: () {
                      var data = {'id': 'switch', 'rs': relay, 'state': 0};
                      AppMasterManager.instance()
                          .sendDataESPDevice(widget._key, data);
                    },
                    child: Text(
                      'ON',
                      style: AppMasterManager.instance().button1,
                    ),
                  ),
                ),
                Expanded(
                  child: OutlineButton(
                    textColor: Colors.green,
                    onPressed: () {
                      var data = {'id': 'switch', 'rs': relay, 'state': 1};
                      AppMasterManager.instance()
                          .sendDataESPDevice(widget._key, data);
                    },
                    child: Text(
                      'OFF',
                      style: AppMasterManager.instance().button1,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //print("This is the Home Profile");
    AppMasterManager.instance().setCurrentTab(0);
    return profileHome();
  }
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
class TimerHomeWidget extends StatefulWidget {
  final String _key;
  TimerHomeWidget(this._key);
  @override
  _TimerHomeWidgetState createState() => _TimerHomeWidgetState();
}

class _TimerHomeWidgetState extends State<TimerHomeWidget> {
  Timer refreshTimer;

  refresh() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    refreshTimer = Timer.periodic(Duration(seconds: 10), (refreshTimer) {
      setState(() {});
    });
    AppMasterManager.instance().listeners['timer'] = refresh;
  }

  @override
  void dispose() {
    super.dispose();
    refreshTimer.cancel();
    AppMasterManager.instance().listeners.remove('timer');
  }

  Widget timerschedHome() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Container(
            decoration: new BoxDecoration(
              border: new Border.all(
                color: Colors.grey,
                width: 2.0,
              ),
              borderRadius: new BorderRadius.all(
                new Radius.circular(5.0),
              ),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: OutlineButton(
                        onPressed: () {
                          AppMasterManager.instance()
                              .deviceESPHolder[widget._key]
                              .outGoingData({'id': 'gettimers'});
                        },
                        child: Text(
                          "REFRESH",
                          style: TextStyle(fontSize: 30.0),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: OutlineButton(
                        onPressed: () async {
                          var result = await Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return NewTimerPage();
                            }),
                          );
                          if (result != null) {
                            print(result.toString());
                            AppMasterManager.instance()
                                .deviceESPHolder[widget._key]
                                .outGoingData(result);
                          }
                        },
                        child: Text(
                          "ADD NEW TIMER",
                          style: TextStyle(fontSize: 30.0),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Container(
                    color: Colors.grey[300],
                    child: _newTimerList(),
                  ),
                  //child: _timerListBuilder(),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget timerInfo(entry, data) {
    return Card(
      elevation: 5.0,
      color: Colors.blue[50],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            timerDetail(entry, data),
            textAlign: TextAlign.center,
          ),
          OutlineButton(
            onPressed: () async {
              var result =
                  await AppMasterManager.instance().createYesOrNo(context);
              if (result != null) {
                print("RESULT : $result");
                if (result == 'YES') {
                  var data = {'id': 'deltimer', 'rs': entry};
                  AppMasterManager.instance()
                      .sendDataESPDevice(widget._key, data);
                }
              }
            },
            child: Text("Delete"),
          ),
        ],
      ),
    );
  }

  Widget _newTimerList() {
    return ListView.builder(
      padding: const EdgeInsets.all(12.0),
      itemCount: AppMasterManager.instance()
          .deviceESPHolder[widget._key]
          .deviceTimers
          .length,
      itemBuilder: (BuildContext context, int index) {
        var entry = AppMasterManager.instance()
            .deviceESPHolder[widget._key]
            .deviceTimers[index];
        var value = AppMasterManager.instance()
            .deviceESPHolder[widget._key]
            .deviceTimersValues[entry];
        return timerInfo(entry, value);
      },
    );
  }

  String timerDetail(entry, data) {
    var repeat = data[2];
    var result = entry.toString();
    var title = '';
    if (result == 'r1on') {
      title = 'RelayA ON';
    } else if (result == 'r1off') {
      title = 'RelayA OFF';
    } else if (result == 'r2on') {
      title = 'RelayB ON';
    } else if (result == 'r2off') {
      title = 'RelayB OFF';
    }

    var detail = '';
    //Constructing Timer/Schedule details
    //if not repeatable
    //Relay A ON - 3:25 PM
    if (repeat == false) {
      var h = data[0];
      var m = data[1];
      var p = '';
      if (h > 11) {
        h = h - 12;
        p = 'PM';
      } else {
        p = 'AM';
      }
      detail = '$h:$m$p';
    } else {
      var ih = data[3];
      var im = data[4];
      detail = 'Repeat $ih hr:$im min';
    }
    //if repeatable
    //Relay A -

    //detail = 'Hour : ' + data[1].toString();

    var hour = '';
    var minute = '';
    var period = '';

    return '$title - $detail';
    //return '$title - $data';
  }

  @override
  Widget build(BuildContext context) {
    print("This is the Timer Page");
    AppMasterManager.instance().setCurrentTab(1);
    return timerschedHome();
  }
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
class SensorHomeWidget extends StatefulWidget {
  final String _key;
  SensorHomeWidget(this._key);
  @override
  _SensorHomeWidgetState createState() => _SensorHomeWidgetState();
}

class _SensorHomeWidgetState extends State<SensorHomeWidget> {
  Timer refreshTimer;

  refresh() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    AppMasterManager.instance().listeners['sensor'] = refresh;
    refreshTimer = Timer.periodic(Duration(seconds: 10), (refreshTimer) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    refreshTimer.cancel();
    AppMasterManager.instance().listeners.remove('sensor');
  }

  Widget sensorPage() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Container(
            decoration: new BoxDecoration(
              border: new Border.all(
                color: Colors.black26,
                width: 5.0,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                sensorNewWidget('SENSOR A', 1),
              ],
            ),
          ),
        ),
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Container(
            decoration: new BoxDecoration(
              border: new Border.all(
                color: Colors.black26,
                width: 5.0,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                sensorNewWidget('SENSOR B', 2),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget sensorNewWidget(txt, port) {
    Widget widget2;
    bool status = AppMasterManager.instance().getSensorState(widget._key, port);
    var values = AppMasterManager.instance().getSensorValues(widget._key, port);
    if (status == false) {
      //get empty ui
      widget2 = new OutlineButton.icon(
        onPressed: () async {
          String temp =
              await AppMasterManager.instance().createSensorType(context);
          if (temp != null) {
            if (temp == 'ag') {
              var res = await Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                //return NewSensorPage(
                return NewAnalogPage(
                  sensorPort: port,
                );
              }));
              if (res != null) {
                if (res[0].toString() == 'activate') {
                  var val = res[1];
                  print(val);
                }
                // if (res[0].toString() == 'activate') {
                //   var values = res[1].sensorValues();
                //   var data = {'id': 'setsensor', 'sensor': values};
                //   AppMasterManager.instance()
                //       .sendDataESPDevice(widget._key, data);
                //   print("ACTIVATE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                //   print(values);
                //   AppMasterManager.instance().onUpdateEvent('sensor');
                // }
              } else {
                print('No data from sensor page.');
              }
            } else if (temp == 'dg') {
              //print('DIGITAL SENSOR PAGE TO BE WRITTEN...');
              var res = await Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                return NewDigitalPage(
                  sensorPort: port,
                );
              }));
            }
          }
        },
        icon: Icon(
          Icons.add_box,
          size: 50,
        ),
        label: Text(
          txt,
          style: TextStyle(fontSize: 30.0),
        ),
      );
    } else {
      //get full ui
      widget2 = new Column(
        children: [
          Text(
            values,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Arial',
              fontSize: 35.0,
            ),
          ),
          OutlineButton(
            onPressed: () async {
              String temp =
                  await AppMasterManager.instance().createYesOrNo(context);
              if (temp == 'YES') {
                print('Removing Sensor at : $port');
                // var data = {'id': 'remsensor', 'port': 1};
                // AppMasterManager.instance()
                //     .sendDataESPDevice(widget._key, data);

                AppMasterManager.instance()
                    .setSensorState(widget._key, port, false);
              } else if (temp == 'NO') {}
            },
            child: Text('DEACTIVATE'),
          ),
        ],
      );
    }
    return widget2;
  }

  @override
  Widget build(BuildContext context) {
    print("SENSOR PAGE");
    AppMasterManager.instance().setCurrentTab(2);
    return sensorPage();
  }
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
class ChargingmodeHomeWidget extends StatefulWidget {
  final String _key;
  ChargingmodeHomeWidget(this._key);

  @override
  _ChargingmodeHomeWidgetState createState() => _ChargingmodeHomeWidgetState();
}

class _ChargingmodeHomeWidgetState extends State<ChargingmodeHomeWidget> {
  var channel = const MethodChannel('com.example/background_service');
  var callbackHandle = PluginUtilities.getCallbackHandle(backgroundMain);
  Timer refreshTimer;

  String phoneID = "";

  refresh() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    //add to appmastermanager as eventlistener
    AppMasterManager.instance().listeners['charging'] = refresh;
    refreshTimer = Timer.periodic(Duration(seconds: 10), (refreshTimer) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    refreshTimer.cancel();
    AppMasterManager.instance().listeners.remove('charging');
  }

  Widget chargeModePage() {
    Color color = Colors.green;
    String chargeText = "";
    if (AppMasterManager.instance()
            .deviceESPHolder[widget._key]
            .inChargingMode ==
        true) {
      color = Colors.red;
      chargeText = "CHARGE-MODE IS ON";
    } else {
      color = Colors.green;
      chargeText = "CHARGE-MODE IS OFF";
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Container(
            decoration: new BoxDecoration(
              border: new Border.all(
                color: Colors.red,
                width: 1.0,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // OutlineButton(
                //   onPressed: () async {
                //     ConfigObject c = new ConfigObject(
                //         AppMasterManager.instance().networkDevices,
                //         AppMasterManager.instance().networkDevicesInfo,
                //         AppMasterManager.instance().profiledDevices,
                //         AppMasterManager.instance().profiledDevicesInfo);
                //     String encoded = jsonEncode(c);
                //     print("SERIALIZED DATA : $encoded");
                //     //Test write to SharedPre
                //     var prefs = await SharedPreferences.getInstance();
                //     prefs.setString('configs', encoded);
                //     print("DONE WRITING TO SHARED");

                //     print("Reading address from Shared Preference");
                //     String sConfig = prefs.getString('configs') ?? '';
                //     print("DONE READING TO SHARED : $sConfig");
                //     Map<String, dynamic> decoded = jsonDecode(sConfig);
                //     print("DESERIALIZED DATA : $decoded");
                //   },
                //   child: Text('TEST SERIALIZING'),
                // ),
                Text(
                  phoneID,
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  chargeText,
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                FlatButton(
                  onPressed: () async {
                    //CHARGE MODE IS ACTIVE
                    if (AppMasterManager.instance()
                            .getChargeMode(widget._key) ==
                        true) {
                      var result = await AppMasterManager.instance()
                          .createYesOrNo(context);
                      if (result != null) {
                        if (result == 'YES') {
                          print("YESSSSSSSSS");
                          AppMasterManager.instance()
                              .clearChargeMode(widget._key);
                          setState(() {});
                          //STOP FOREGROUND SERVICE
                          ChargingModeService.instance().stopMonitor();

                          var data = {'id': 'chargeoff'};
                          //var data = {'id': 'switch', 'rs': relay, 'state': 0};
                          AppMasterManager.instance()
                              .sendDataESPDevice(widget._key, data);
                          //update device for charging mode off
                        } else {
                          print("NOOOOOOOO");
                        }
                      }
                    } else //CHARGE MODE IS NO ACTIVE TURNING ON NOW
                    {
                      var result = await Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return NewChargeMode(widget._key);
                        }),
                      );
                      if (result != null) {
                        //print(result.toString());
                        var cp = AppMasterManager.instance()
                            .deviceESPHolder[widget._key]
                            .chargingPoint;
                        //START FOREGROUND SERVICE
                        var dKey = widget._key;
                        //Send a Chargemode on at DEVICE
                        var data = {
                          'id': 'chargeon',
                          'cpoint': cp,
                          'user': AppMasterManager.instance().appUser
                        };
                        AppMasterManager.instance()
                            .sendDataESPDevice(dKey, data);

                        //Next is start the background-service
                        channel.invokeMethod(
                            'startService', callbackHandle.toRawHandle());
                        //ChargingModeService.instance().deviceName = 'test device';
                        //ChargingModeService.instance().deviceAddress = '192.168.254.102';
                        ChargingModeService.instance().setAddress(
                            AppMasterManager.instance()
                                .getDeviceAddress(widget._key));

                        ChargingModeService.instance().setChargingPoint(cp);
                        ChargingModeService.instance().chargingMonitor();
                      }
                    }
                  },
                  splashColor: Colors.white,
                  child: Icon(
                    Icons.charging_station_rounded,
                    size: 450.0,
                    color: color,
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    AppMasterManager.instance().setCurrentTab(3);
    return chargeModePage();
  }
}
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
