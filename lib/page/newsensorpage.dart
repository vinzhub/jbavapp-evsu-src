import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class SensorESP {
  String name = "Default";
  String sensorType = ''; //AG or DG
  String relayToSwitch = 'A'; //Relay A or B
  int type = 1;
  int port = 1;
  int onVal = 0;
  int offVal = 0;

  void info() {
    print("NAME : $name  TYPE : $type  PORT : $port");
  }

  sensorValues() {
    var t = "";
    var p = "";
    //Match device server packet structure
    if (type == 1) {
      t = "monitor";
    } else {
      t = "switch";
    }

    if (port == 1) {
      p = "A";
    } else {
      p = "B";
    }

    return [name, t, p, onVal, offVal, sensorType, relayToSwitch];
    //       0    1  2    3       4         5            6
  }
}

class NewSensorPage extends StatefulWidget {
  final int sensorPort;

  const NewSensorPage({Key key, this.sensorPort}) : super(key: key);

  @override
  _NewSensorPageState createState() => _NewSensorPageState();
}

class _NewSensorPageState extends State<NewSensorPage> {
  TextEditingController sensorName = TextEditingController();
  SensorESP sensorEsp = SensorESP();

  @override
  void initState() {
    super.initState();
    sensorEsp.port = widget.sensorPort;
    sensorEsp.sensorType = 'AG';
  }

  void handleSensorType(int value) {
    setState(() {
      sensorEsp.type = value;
    });
  }

  //#TODO:
  void addSensor() {
    //sensor Port A or B
    //sensor name
    //sensor type Monitor or switch
    //sensor on value
    //sensor off valu
  }
  //#TODO:
  void removeSensor() {
    //sensor Port A or B
    //sensor name
  }

  Widget sensorData() {
    return TextFormField(
      decoration: InputDecoration(
        filled: true,
        labelText: "Sensor Name",
      ),
      onChanged: (value) {
        sensorEsp.name = value;
      },
    );
  }

  Widget sensorValues() {
    return Column(
      children: [
        TextFormField(
          decoration: InputDecoration(
            labelText: "ON Value",
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          onChanged: (value) {
            sensorEsp.onVal = int.parse(value);
          },
        ),
        TextFormField(
          decoration: InputDecoration(
            labelText: "OFF Value",
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          onChanged: (value) {
            sensorEsp.offVal = int.parse(value);
          },
        ),
      ],
    );
  }

  Widget sensorType() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Monitor'),
        Radio<int>(
          value: 1,
          groupValue: sensorEsp.type,
          onChanged: handleSensorType,
        ),
        SizedBox(
          width: 10.0,
        ),
        Text('Monitor/Switch'),
        Radio<int>(
          value: 2,
          groupValue: sensorEsp.type,
          onChanged: handleSensorType,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    const sizedBoxSpace = SizedBox(height: 24);
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        leading: new Container(
          child: Icon(Icons.sensor_door),
        ),
        title: Text('SETUP SENSOR : ${widget.sensorPort}'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            sensorData(),
            //sizedBoxSpace,
            sensorType(),
            //sizedBoxSpace,
            sensorValues(),
            OutlineButton(
              onPressed: () {
                sensorEsp.info();
                //Navigator.pop(context, "TEST ${widget.sensorPort}");
                Navigator.pop(context, ["activate", sensorEsp]);
              },
              child: Text("ACTIVATE SENSOR"),
            ),
          ],
        ),
      ),
    );
  }
}
