import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:jbavapp/utils/appmastermgr.dart';

class ScanDevicesPage extends StatefulWidget {
  @override
  _ScanDevicesPageState createState() => _ScanDevicesPageState();
}

class _ScanDevicesPageState extends State<ScanDevicesPage> {
  Timer refreshScan;
  void refresh() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    refreshScan = Timer.periodic(
      Duration(seconds: 5),
      (refresher) {
        print("NEW SCAN PAGE CALLED / TIMER");
        refresh();
      },
    );

    //Check appmanager if scanning is active or not
    if (AppMasterManager.instance().isScanning == false) {
      AppMasterManager.instance().initScanning();
      print('Scan page loaded');
    } else {
      print("Already scanning");
    }
  }

  @override
  void dispose() {
    super.dispose();
    refreshScan.cancel();
    print("Timer Cancelled");
  }

  Widget scanList() {
    return new ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: AppMasterManager.instance().networkDevices.length,
      itemBuilder: (BuildContext context, int index) {
        var key = AppMasterManager.instance().networkDevices[index];
        var addr = AppMasterManager.instance().networkDevicesInfo[key];
        return ScanDeviceInfo(key, addr);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          '- SCANNING NETWORK DEVICES -',
          style: TextStyle(
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(
          height: 8.0,
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.all(8.0),
            child: scanList(),
          ),
        ),
      ],
    );
  }
}

class ScanDeviceInfo extends StatelessWidget {
  final name;
  final address;
  ScanDeviceInfo(this.name, this.address);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        splashColor: Colors.blue[300],
        onLongPress: () async {
          print("DELETE THIS SCAN???");
          String res = await AppMasterManager.instance().createYesOrNo(context);
          if (res != null) {
            if (res == 'YES') {
              AppMasterManager.instance().networkDevices.remove(name);
              AppMasterManager.instance().networkDevicesInfo.remove(name);
            }
          }
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.lightBlue[100],
          ),
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 3,
                    child: Text('Name : $name Addr: $address'),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 2,
                    child: OutlineButton(
                      onPressed: () async {
                        //Open an Alert
                        String temp = await AppMasterManager.instance()
                            .createDialog(context);
                        if (temp != null) {
                          AppMasterManager.instance()
                              .addProfiledDevice(temp, address);
                        }
                      },
                      child: Text('ADD'),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
