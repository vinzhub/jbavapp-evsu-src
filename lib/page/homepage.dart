import 'package:flutter/material.dart';
import 'package:jbavapp/page/profilepage.dart';
import 'package:jbavapp/utils/appmastermgr.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void refresh() {
    setState(() {});
  }

  @override
  void dispose(){
    super.dispose();
  }

  List<String> hello = [];
  @override
  void initState() {
    super.initState();
    //AppManager.instance().addDevice('KITCHEN');
    //AppMasterManager.instance().loadConfig();
    print("Home Screen Called");
  }

  Widget gridListDevices() {
    return new GridView.builder(
      itemCount: AppMasterManager.instance().profiledDevices == null
          ? 0 
          : AppMasterManager.instance().profiledDevices.length,
      gridDelegate:
          new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index) {
        var entry = AppMasterManager.instance().profiledDevices[index];
        return ProfilePage(entry, refresh);
      },
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: 10.0,
          ),
        ],
      ),
      Expanded(
        child: Container(
          color: Colors.white12,
          margin: EdgeInsets.all(8.0),
          child: gridListDevices(),
        ),
      ),
    ]);
  }

}