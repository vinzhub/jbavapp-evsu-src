import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
//import 'package:jbavapp/main/appmanager.dart';
import 'package:intl/intl.dart';
import 'package:jbavapp/utils/appmastermgr.dart';

class _Menu extends StatelessWidget {
  const _Menu({
    Key key,
    @required this.children,
  })  : assert(children != null),
        super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
            //top: BorderSide(color: CupertinoColors.inactiveGray, width: 0),
            //bottom: BorderSide(color: CupertinoColors.inactiveGray, width: 0),
            ),
      ),
      height: 44,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: children,
        ),
      ),
    );
  }
}

class _BottomPicker extends StatelessWidget {
  const _BottomPicker({
    Key key,
    @required this.child,
  })  : assert(child != null),
        super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 216,
      padding: const EdgeInsets.only(top: 6),
      margin: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      color: CupertinoColors.systemBackground.resolveFrom(context),
      child: DefaultTextStyle(
        style: TextStyle(
          color: CupertinoColors.label.resolveFrom(context),
          fontSize: 22,
        ),
        child: GestureDetector(
          // Blocks taps from propagating to the modal sheet and popping.
          onTap: () {},
          child: SafeArea(
            top: false,
            child: child,
          ),
        ),
      ),
    );
  }
}

void _showDemoPicker({
  @required BuildContext context,
  @required Widget child,
}) {
  final themeData = CupertinoTheme.of(context);
  final dialogBody = CupertinoTheme(
    data: themeData,
    child: child,
  );

  showCupertinoModalPopup<void>(
    context: context,
    builder: (context) => dialogBody,
  );
}

class NewTimerPage extends StatefulWidget {
  @override
  _NewTimerPageState createState() => _NewTimerPageState();
}

class _NewTimerPageState extends State<NewTimerPage> {
  bool _switch = false; //On or Off
  bool _repeat = false; //On or Off
  DateTime time = DateTime.now();
  TimeOfDay timeDate = TimeOfDay.now();
  DateTime time2 = DateTime.now();
  Duration timer = const Duration();
  //Relay 1 or Relay 2
  int relayValue = 1;

  DateTime formatTimeOfDay() {
    return DateTime(
        time.year, time.month, time.day, timeDate.hour, timeDate.minute);
  }

  void handleRelayChange(int value) {
    setState(() {
      relayValue = value;
    });
  }

  Future<void> _showTimePicker(BuildContext context) async {
    final picked = await showTimePicker(
      context: context,
      initialTime: timeDate,
    );
    if (picked != null && picked != timeDate) {
      setState(() {
        print("-----------------------------------");
        print("Current Time Date :$timeDate");
        print("Picked Time :$picked");
        print("-----------------------------------");
        timeDate = picked;
      });
    }
  }

  Widget _buildCountdownTimerPicker(BuildContext context) {
    return Container(
      height: 50.0,
      child: GestureDetector(
        onTap: () {
          _showDemoPicker(
            context: context,
            child: _BottomPicker(
              child: CupertinoTimerPicker(
                backgroundColor:
                    CupertinoColors.systemBackground.resolveFrom(context),
                initialTimerDuration: timer,
                onTimerDurationChanged: (newTimer) {
                  setState(() {
                    if (newTimer != null) {
                      timer = newTimer;
                    }
                  });
                },
              ),
            ),
          );
        },
        child: _Menu(
          children: [
            Text(
              '${timer.inHours}:'
              '${(timer.inMinutes % 60).toString().padLeft(2, '0')}:'
              '${(timer.inSeconds % 60).toString().padLeft(2, '0')}',
              style: const TextStyle(
                fontSize: 35.0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _showTimeUI() {
    return Container(
      child: Text(
        DateFormat.jm().format(formatTimeOfDay()),
        //timeDate.toString(),
        style: const TextStyle(
          color: CupertinoColors.black,
          fontSize: 35.0,
        ),
      ),
    );
  }

  Widget _repeatSwitch() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('REPEATABLE ON/OFF: '),
        CupertinoSwitch(
          value: _repeat,
          onChanged: (value) {
            setState(() {
              _repeat = value;
            });
          },
        ),
      ],
    );
  }

  Widget _switchType() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('SWITCH TYPE ON/OFF: '),
        CupertinoSwitch(
          value: _switch,
          onChanged: (value) {
            setState(() {
              _switch = value;
            });
          },
        ),
      ],
    );
  }

  Widget _relaySwitch() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Relay 1'),
        Radio<int>(
          value: 1,
          groupValue: relayValue,
          onChanged: handleRelayChange,
        ),
        SizedBox(
          width: 10.0,
        ),
        Text('Relay 2'),
        Radio<int>(
          value: 2,
          groupValue: relayValue,
          onChanged: handleRelayChange,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        title: Text('New Timer/Task/Schedule'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            _relaySwitch(),
            OutlineButton(
              onPressed: () async {
                await _showTimePicker(context);
              },
              child: Text('Select Time'),
            ),
            _showTimeUI(),
            _switchType(),
            _repeatSwitch(),
            Text(
              'INTERVAL:',
              style: TextStyle(
                fontSize: 18.0,
              ),
            ),
            _buildCountdownTimerPicker(context),
            OutlineButton(
              onPressed: () async {
                //print('ADDING......');
                // String temp = await AppMasterManager.instance().createYesOrNo(context);
                // if(temp != null){
                //   if(temp == 'YES'){
                //     print('ADDING NOW');
                //     Navigator.pop(context);
                //   }
                //   else{
                //     print('CANCELLED');
                //   }
                // }
                String temp =
                    await AppMasterManager.instance().createYesOrNo(context);
                if (temp != null) {
                  if (temp == "YES") {
                    //Timer format
                    //{'id':'settimer', 'rs':'r1on or r1off', 'value':[hour, minutes, repeatable, hour_interval, minute_interval, 'r1 or r2', True or False]}
                    var relay = '';
                    var relayNo = ''; //'r1' : 'r2'
                    var switchtype; //True or False
                    if (relayValue == 1) {
                      relayNo = 'r1';
                      if (_switch == true) {
                        relay = 'r1on';
                        switchtype = true;
                      } else {
                        relay = 'r1off';
                        switchtype = false;
                      }
                    } else if (relayValue == 2) {
                      relayNo = 'r2';
                      if (_switch == true) {
                        relay = 'r2on';
                        switchtype = true;
                      } else {
                        relay = 'r2off';
                        switchtype = false;
                      }
                    }
                    var repeatable = _repeat;
                    var _data = {
                      'id': 'settimer',
                      'rs': relay,
                      'value': [
                        timeDate.hour,
                        timeDate.minute,
                        repeatable,
                        timer.inHours,
                        timer.inMinutes - (timer.inHours * 60),
                        relayNo,
                        switchtype
                      ]
                    };
                    Navigator.pop(context, _data);
                  } else {
                    //Dont do anything
                  }
                }
              },
              child: Text(
                'Add Timer',
                style: TextStyle(fontSize: 38),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
