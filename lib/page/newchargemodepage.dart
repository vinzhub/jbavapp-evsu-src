import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'newsensorpage.dart';
import 'newtimerpage.dart';
import 'package:jbavapp/utils/appmastermgr.dart';
import 'package:wakelock/wakelock.dart';

class NewChargeMode extends StatefulWidget {
  final String _key;
  NewChargeMode(this._key);
  @override
  _NewChargeModeState createState() => _NewChargeModeState();
}

class _NewChargeModeState extends State<NewChargeMode> {
  //Charging
  int chRelayvalue = 1; //What relay to be set in charging mode
  String chPoint = "";
  TextEditingController _chPoint = TextEditingController();

  // #4 Charging Mode
  void handleRelayChange(int value) {
    setState(() {
      chRelayvalue = value;
    });
  }

  Widget home() {
    Widget cur;
    cur = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("CHARGING MAX POINT %:"),
          ],
        ),
        TextFormField(
          decoration: InputDecoration(
            labelText: "CHARGING POINT",
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          onChanged: (value) {
            chPoint = value;
            //sensorEsp.onVal = int.parse(value);
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text('Relay 1'),
            Radio<int>(
              value: 1,
              groupValue: chRelayvalue,
              onChanged: handleRelayChange,
            ),
            Text('Relay 2'),
            Radio<int>(
              value: 2,
              groupValue: chRelayvalue,
              onChanged: handleRelayChange,
            ),
          ],
        ),
        OutlineButton(
          onPressed: () {
            AppMasterManager.instance()
                .setChargeMode(widget._key, chPoint, chRelayvalue);
            //setChargeMode
            Navigator.pop(context, "CHARGE MODE SET");
          },
          child: Text("SAVE"),
        ),
      ],
    );

    return cur;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        leading: new Container(
          child: Icon(Icons.sensor_door),
        ),
        title: Text('CHARGE MODE'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            home(),
          ],
        ),
      ),
    );
  }
}
