import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:jbavapp/utils/appmastermgr.dart';

class ConfigPage extends StatefulWidget {
  @override
  _ConfigPageState createState() => _ConfigPageState();
}

class _ConfigPageState extends State<ConfigPage> {
  List<String> deviceConfig = [];
  Map<String, String> deviceConfigMap = {};

  Socket deviceSocket;

  void refresh() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    deviceConfig = AppMasterManager.instance().networkDevices;
    deviceConfigMap = AppMasterManager.instance().networkDevicesInfo;
    print('OPEN CONFIG PAGE');
  }

  @override
  void dispose() {
    super.dispose();
    print('CLOSE CONFIG PAGE');
    if (deviceSocket != null) {
      deviceSocket.close();
      print('SOCKET CLOSED!');
    }
  }

  Widget configPage(entry) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      color: Colors.grey[200],
      shape: RoundedRectangleBorder(
        //borderRadius: BorderRadius.circular(120.0),
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(120.0)),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
      child: InkWell(
        splashColor: Colors.blue[300],
        onLongPress: () async {},
        onTap: () async {
          var result = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return ConfigInnerPage(entry);
            }),
          );
          if (result != null) {
            if (result[0] == true) {
              //prep data
              var values = result[1];
              var data = {'id': 'config', 'data': values};
              //prep socket to connect
              var ip = deviceConfigMap[entry];
              print('Data : $data  to be sent to  : $ip');

              //start sending
              if (data['ssid'] != ' ') {
                deviceSocket = await Socket.connect(ip, 80);
                deviceSocket.timeout(Duration(seconds: 3));
                print('PACKET SENT!');
                Timer(Duration(seconds: 5), () async {
                  print('...');
                  var encodedData = jsonEncode(data);
                  encodedData = encodedData + '\n';
                  deviceSocket.add(utf8.encode(encodedData));
                  deviceSocket.flush();
                  print('Socket flushed!');
                  //remove device from current and add it later after it reboot
                  setState(() {
                    deviceConfig.remove(entry);
                    deviceConfigMap.remove(entry);
                  });
                });
                Timer(Duration(seconds: 12), () {
                  deviceSocket.close();
                  print('SOCKET CLOSED IN LAST TIMER');
                });
              } else {
                print('NOT SENDING!!!');
              }
            }
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              entry,
              style: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget gridListDevices() {
    return new GridView.builder(
      itemCount: deviceConfig == null ? 0 : deviceConfig.length,
      gridDelegate:
          new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index) {
        var entry = deviceConfig[index];
        return configPage(entry);
      },
      scrollDirection: Axis.vertical,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 50.0,
          ),
          Text(
            "- DEVICE CONFIGURATIONS -",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
            ),
          ),
        ],
      ),
      Expanded(
        child: Container(
          color: Colors.white12,
          margin: EdgeInsets.all(8.0),
          child: gridListDevices(),
        ),
      ),
    ]);
  }
}

class ConfigInnerPage extends StatefulWidget {
  final String entry;
  ConfigInnerPage(this.entry);
  @override
  _ConfigInnerPageState createState() => _ConfigInnerPageState();
}

class _ConfigInnerPageState extends State<ConfigInnerPage> {
  int wifiMode = 0; //0 for Station : 1 for Access Point
  String ssid = " ";
  String pwd = " ";
  TextEditingController ssidTxt = TextEditingController();
  TextEditingController pwdTxt = TextEditingController();

  void onWifiModeChange(int index) {
    setState(() {
      wifiMode = index;
    });
    print('CHANGED! $index');
  }

  Widget ssidWidget() {
    return TextFormField(
      decoration: InputDecoration(
        filled: true,
        labelText: "Wifi SSID :",
      ),
      onChanged: (value) {
        ssid = value;
      },
    );
  }

  Widget pwdWidget() {
    return TextFormField(
      decoration: InputDecoration(
        filled: true,
        labelText: "Wifi PWD :",
      ),
      onChanged: (value) {
        pwd = value;
      },
    );
  }

  Widget wifiModeWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text('STA :'),
            Radio<int>(
              value: 0,
              groupValue: wifiMode,
              onChanged: onWifiModeChange,
            ),
            SizedBox(
              width: 10.0,
            ),
            Text('AP :'),
            Radio<int>(
              value: 1,
              groupValue: wifiMode,
              onChanged: onWifiModeChange,
            ),
          ],
        ),
        SizedBox(
          width: 10.0,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        leading: new Container(
          child: Icon(Icons.sensor_door),
        ),
        title: Text('DEVICE CONFIGURATIONS'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text(
              widget.entry,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 45,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "DEVICE WIFI MODE",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            wifiModeWidget(),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "CREDENTIALS",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            ssidWidget(),
            pwdWidget(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: OutlineButton(
                    onPressed: () {
                      //prepare values
                      var res = {};
                      res['ssid'] = ssid;
                      res['pwd'] = pwd;
                      res['mode'] = wifiMode;
                      Navigator.pop(context, [true, res]);
                    },
                    child: Text("SAVE"),
                  ),
                ),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: OutlineButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("CANCEL"),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
