import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class SensorESP2 {
  int sensorPin = 0;
  String name = "default";
  int sensorMode = 0;
  String sensorType = "";
  String sensorAction = "";
  String sensorActRelay = "A";
  int sensorTriggerValue = 1000; //Default for max

  getValues() {
    var data = {
      'sensorpin': sensorPin,
      'name': name,
      'mode': sensorMode,
      'type': sensorType,
      'action': sensorAction,
      'actrelay': sensorActRelay,
      'trigvalue': sensorTriggerValue
    };
    return data;
  }
}

class NewAnalogPage extends StatefulWidget {
  final int sensorPort;

  const NewAnalogPage({Key key, this.sensorPort}) : super(key: key);

  @override
  _NewAnalogPageState createState() => _NewAnalogPageState();
}

class _NewAnalogPageState extends State<NewAnalogPage> {
  TextEditingController name = TextEditingController();
  SensorESP2 sensorInfo = SensorESP2();
  bool showActRelay = false;
  int sensorRelays = 0;
  int sensorAction = 0;

  void onSensorModeChange(int value) {
    //0 is SWITCH   1 is MONITOR
    setState(() {
      sensorInfo.sensorMode = value;
    });
  }

  void onSensorActRelayChange(int value) {
    setState(() {
      sensorRelays = value;
    });
    if (value == 0) {
      sensorInfo.sensorActRelay = "A";
      sensorRelays = 0;
      //Relay A
    } else if (value == 1) {
      //Relay B
      sensorInfo.sensorActRelay = "B";
    }
  }

  void onSensorActionChange(int value) {
    setState(() {
      sensorAction = value;
    });
    if (value == 0) {
      //ON
      sensorInfo.sensorAction = "ON";
      print('ON ON');
    } else if (value == 1) {
      //OFF
      sensorInfo.sensorAction = "OFF";
      print('OFF OFF');
    }
  }

  Widget firstField() {
    return TextFormField(
      decoration: InputDecoration(
        filled: true,
        labelText: "Sensor Name",
      ),
      onChanged: (value) {
        sensorInfo.name = value;
      },
    );
  }

  Widget secondField() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('MONITOR'),
        Radio<int>(
          value: 1,
          groupValue: sensorInfo.sensorMode,
          onChanged: onSensorModeChange,
        ),
        SizedBox(
          width: 10.0,
        ),
        Text('MONITOR&SWITCH'),
        Radio<int>(
          value: 0,
          groupValue: sensorInfo.sensorMode,
          onChanged: onSensorModeChange,
        ),
      ],
    );
  }

  Widget thirdField() {
    Widget temp;
    //if sensor mode is in Switch mode display this widget
    if (sensorInfo.sensorMode == 0) {
      temp = new Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('RELAY A'),
          Radio<int>(
            value: 0,
            groupValue: sensorRelays,
            onChanged: onSensorActRelayChange,
          ),
          SizedBox(
            width: 10.0,
          ),
          Text('RELAY B'),
          Radio<int>(
            value: 1,
            groupValue: sensorRelays,
            onChanged: onSensorActRelayChange,
          ),
        ],
      );
    } else {
      temp = new SizedBox(
        width: 10.0,
      );
    }
    return temp;
  }

  Widget thirdField2() {
    Widget temp;
    if (sensorInfo.sensorMode == 0) {
      temp = new Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('ON'),
          Radio<int>(
            value: 0,
            groupValue: sensorAction,
            onChanged: onSensorActionChange,
          ),
          SizedBox(
            width: 10.0,
          ),
          Text('OFF'),
          Radio<int>(
            value: 1,
            groupValue: sensorAction,
            onChanged: onSensorActionChange,
          ),
        ],
      );
    } else {
      temp = SizedBox(
        width: 10.0,
      );
    }
    return temp;
  }

  Widget fourthField() {
    return Column(
      children: [
        TextFormField(
          decoration: InputDecoration(
            labelText: "Trigger Value",
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          onChanged: (value) {
            sensorInfo.sensorTriggerValue = int.parse(value);
          },
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    //Match sensor selected port/pin in app to device port
    sensorInfo.sensorPin = widget.sensorPort;
    //match sensor type selected in app
    sensorInfo.sensorType = "AG";

    sensorInfo.sensorMode = 1; //Monitor default
    sensorInfo.sensorAction = "ON"; //default OFF
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        leading: new Container(
          child: Icon(Icons.sensor_door),
        ),
        title: Text('SETUP SENSOR : ${widget.sensorPort}'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            firstField(),
            secondField(),
            thirdField(),
            thirdField2(),
            fourthField(),
            SizedBox(
              height: 5.0,
            ),
            OutlineButton(
              onPressed: () {
                Navigator.pop(context, ["activate", sensorInfo.getValues()]);
              },
              child: Text('CONFIRM'),
            ),
          ],
        ),
      ),
    );
  }
}

class NewDigitalPage extends StatefulWidget {
  final int sensorPort;

  const NewDigitalPage({Key key, this.sensorPort}) : super(key: key);
  @override
  _NewDigitalPageState createState() => _NewDigitalPageState();
}

class _NewDigitalPageState extends State<NewDigitalPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        leading: new Container(
          child: Icon(Icons.sensor_door),
        ),
        title: Text('SETUP SENSOR : ${widget.sensorPort}'),
      ),
      body: Center(
        child: IntrinsicWidth(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(
                height: 25.0,
              ),
              Text(
                "** SUPPORTED SENSORS **",
                style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              OutlineButton(
                onPressed: () {},
                child: Text(
                  "DHT11",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              OutlineButton(
                onPressed: () {},
                child: Text(
                  "MQ135 GAS",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              OutlineButton(
                onPressed: () async {},
                child: Text(
                  "PIR MOTION",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              OutlineButton(
                onPressed: () {},
                child: Text(
                  "IR PROXIMITY",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              OutlineButton(
                onPressed: () {},
                child: Text(
                  "CAPACITATIVE TOUCH",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              OutlineButton(
                onPressed: () {},
                child: Text(
                  "FLAME SENSOR",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              OutlineButton(
                onPressed: () {},
                child: Text(
                  "RAIN SENSOR",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

////////////////////////
//DIGITAL SENSOR PAGES//
class DHT11Page extends StatefulWidget {
  @override
  _DHT11PageState createState() => _DHT11PageState();
}

class _DHT11PageState extends State<DHT11Page> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class MQ135Page extends StatefulWidget {
  @override
  _MQ135PageState createState() => _MQ135PageState();
}

class _MQ135PageState extends State<MQ135Page> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class BasicDigitalPage extends StatefulWidget {
  final String sensorName;
  final int sensorPort;
  const BasicDigitalPage({Key key, this.sensorPort, this.sensorName}) : super(key: key);
  @override
  _BasicDigitalPageState createState() => _BasicDigitalPageState();
}

class _BasicDigitalPageState extends State<BasicDigitalPage> {
  SensorESP2 sensorInfo = SensorESP2();
  bool showActRelay = false;
  int sensorRelays = 0;
  int sensorAction = 0;

  void onSensorModeChange(int value){
    //SWITCH OR MONITOR
  }

  void onSensorActRelayChange(int value){
    //RELAY A OR B
  }

  void onSensorActionChange(int value){
    //ON OR OFF
  }

  void onSensorTriggerChange(int value){
    //LOW OR HIGH
  }

  @override
  void initState() {
    super.initState();
    //Match sensor selected port/pin in app to device port
    sensorInfo.sensorPin = widget.sensorPort;
    //match sensor type selected in app
    sensorInfo.name = widget.sensorName;
    sensorInfo.sensorType = "DG";
    sensorInfo.sensorMode = 1; //Monitor default
    sensorInfo.sensorAction = "ON"; //default OFF
  }  

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
