import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:jbavapp/page/newchargemodepage.dart';
import 'package:jbavapp/page/profilewidgets.dart';
import 'package:jbavapp/utils/chargingmode.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'newsensorpage.dart';
import 'newtimerpage.dart';
import 'package:jbavapp/utils/appmastermgr.dart';
import 'package:wakelock/wakelock.dart';

/* 
TOP PAGE
THE SIMPLE CARD ICON THINGY
*/
class ProfilePage extends StatefulWidget {
  final String profileName;
  final Function owner;
  ProfilePage(this.profileName, this.owner);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String pName;

  @override
  void initState() {
    super.initState();
    pName = widget.profileName;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.blue[100],
      child: InkWell(
        splashColor: Colors.blue[300],
        onLongPress: () async {
          print("DELETE THIS PROFILE???");
          String res = await AppMasterManager.instance().createYesOrNo(context);
          if (res != null) {
            if (res == 'YES') {
              AppMasterManager.instance().removeProfiledDevice(pName);
              widget.owner();
            }
          }
        },
        onTap: () {
          print('Opening ProfileDetail');
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return ProfileInnerPage(pName);
            }),
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              pName,
              style: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/* 
AN INNER PAGE
AADA AN CONTROLS
SWITCHES, TIMERS/SCHEDULES, SENSORS
*/
class ProfileInnerPage extends StatefulWidget {
  final String profileName;
  ProfileInnerPage(this.profileName);
  @override
  _ProfileInnerPageState createState() => _ProfileInnerPageState();
}

class _ProfileInnerPageState extends State<ProfileInnerPage>
    with TickerProviderStateMixin {
  //#################################################################//
  //#################################################################//
  //CLASS VARIABLES
  TabController tabController;
  DateTime time = DateTime.now();
  String _key;
  Timer refresher;
  bool isServerUp = false;
  
  //profilewidgets
  ProfileHomeWidget phw;
  TimerHomeWidget thw;
  SensorHomeWidget shw;
  ChargingmodeHomeWidget chw;

  //#################################################################//
  //#################################################################//
  //OVERRIDE METHODS/FUNCTIONS
  @override
  void initState() {
    super.initState();
    //AppMasterManager.instance().loadConfig();
    //test config

    _key = widget.profileName;
    connectToDevice();

    refresher = Timer.periodic(
      Duration(seconds: 15),
      (refresher) {
        //EspDeviceInfo esp = AppManager.instance().getDevice(widget.profName);
        var address =
            AppMasterManager.instance().getProfiledDeviceAddress(_key);
        checkDeviceConnect(address);
      },
    );

    print("INIT STATE!");

    //Stop sleeping
    bool enable = true;
    Wakelock.toggle(enable: enable);
  }

  @override
  void dispose() {
    super.dispose();
    refresher.cancel();

    bool result = AppMasterManager.instance().isInDevice(_key);
    if (result == true) {
      AppMasterManager.instance().setActiveESPObject(_key, false);
    }
    AppMasterManager.instance().deviceESPHolder[_key].closeSocket();
    print("DISPOSED!");

    AppMasterManager.instance().clearListeners();

    //AppMasterManager.instance().saveConfig();
    bool enable = false;
    Wakelock.toggle(enable: enable);
  }

  void onTabChange(){
    tabController.index = AppMasterManager.instance().currentTab;
  }

  @override
  Widget build(BuildContext context) {
    //AppMasterManager.instance().tabController = new TabController(length: 4, vsync: this);
    tabController = new TabController(length: 4, vsync: this);
    tabController.index = AppMasterManager.instance().currentTab;
    tabController.addListener(onTabChange);
    var tabBarItem = new TabBar(
      tabs: [
        new Tab(
          icon: new Icon(Icons.important_devices),
        ),
        new Tab(
          icon: new Icon(Icons.timer),
        ),
        new Tab(
          icon: new Icon(Icons.sensor_window),
        ),
        new Tab(
          icon: new Icon(Icons.battery_alert),
        ),
      ],
      controller: tabController,
      indicatorColor: Colors.white,
      onTap: (index){
        //Handle current setting of tab
        print('clicked index $index');
        AppMasterManager.instance().setCurrentTab(index);
        //AppMasterManager.instance().currentTab = index;
      },
    );
    return new DefaultTabController(
      length: 4,
      child: new Scaffold(
        appBar: new AppBar(
          automaticallyImplyLeading: false,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text(
                    widget.profileName.toUpperCase(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0,
                    ),
                  ),
                  Text(
                    '_dAddr',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 10.0,
                    ),
                  ),
                ],
              ),
            ],
          ),
          bottom: tabBarItem,
        ),
        body: new TabBarView(
          controller: tabController,
          //controller: AppMasterManager.instance().tabController,
          children: [
            new ProfileHomeWidget(_key),
            new TimerHomeWidget(_key),
            new SensorHomeWidget(_key),
            new ChargingmodeHomeWidget(_key),
          ],
        ),
      ),
    );
  }
  //#################################################################//

  //################################################
  String timerDetail(entry, data) {
    var repeat = data[2];
    var result = entry.toString();
    var title = '';
    if (result == 'r1on') {
      title = 'RelayA ON';
    } else if (result == 'r1off') {
      title = 'RelayA OFF';
    } else if (result == 'r2on') {
      title = 'RelayB ON';
    } else if (result == 'r2off') {
      title = 'RelayB OFF';
    }

    var detail = '';
    //Constructing Timer/Schedule details
    //if not repeatable
    //Relay A ON - 3:25 PM
    if (repeat == false) {
      var h = data[0];
      var m = data[1];
      var p = '';
      if (h > 11) {
        h = h - 12;
        p = 'PM';
      } else {
        p = 'AM';
      }
      detail = '$h:$m$p';
    } else {
      var ih = data[3];
      var im = data[4];
      detail = 'Repeat $ih hr:$im min';
    }
    //if repeatable
    //Relay A -

    //detail = 'Hour : ' + data[1].toString();

    var hour = '';
    var minute = '';
    var period = '';

    return '$title - $detail';
    //return '$title - $data';
  }
  //################################################

  //#################################################################//
  //HELPER METHODS
  void syncAppDeviceTime() {
    print("APP AND DEVICE TIME SYNCING IN PROCESS...");
    var data = {
      'id': 'synctime',
      'year': time.year,
      'month': time.month,
      'day': time.day,
      'hour': time.hour,
      'minute': time.minute,
      'second': time.second
    };
    AppMasterManager.instance().sendDataESPDevice(_key, data);
  }

  void refresh() {
    //setState(() {});
  }

  void refreshTab(tab) {
    if (this.mounted) {
      refresh();
      Timer(Duration(milliseconds: 100), () {
        //print('ANIMATING');
        tabController.animateTo(tab);
      });
    }
  }

  void onNotificationHandler() {
    //print("NOTIFICATION RECEIVED!");
    refresh();
  }
  //#################################################################//

  //#################################################################//
  // NETWORK FUNCTIONS
  void connectToDevice() {
    //Get device address from profiledDevices
    var address = AppMasterManager.instance().getProfiledDeviceAddress(_key);
    print('ADDRESS : $address');
    //Check if device is already in AppManagers DeviceESPHolder
    bool result = AppMasterManager.instance().isInDevice(_key);

    //IF device is already added to DeviceESPHolder
    //Reactivate it
    if (result == true) {
      print('REACTIVATING ESPOBJECT...');
      AppMasterManager.instance().setActiveESPObject(_key, true);
      AppMasterManager.instance().setEventHandler(_key, onNotificationHandler);
      AppMasterManager.instance().activateESPObject(_key);

      //then call synctime after 10 seconds
      Timer(Duration(seconds: 10), () {
        print('Synching time after 10 seconds');
        syncAppDeviceTime();
      });

      AppMasterManager.instance().getInitData(_key, {'id': 'index'});

      // var r =
      //     AppMasterManager.instance().deviceESPHolder[_key].isSocketConnected;
      // if (r == true) {
      //   print('REACTIVATION ESPOBJECT SUCCESSFUL!');
      // } else {
      //   print('REACTIVATION ESPOBJECT FAILED!');
      // }
    } else {
      print('INITIALIZING ESPOBJECT...');
      AppMasterManager.instance()
          .addDeviceESP(_key, new ESPObject(_key, address));
      AppMasterManager.instance().setEventHandler(_key, onNotificationHandler);
      AppMasterManager.instance().activateESPObject(_key);

      //then call synctime after 10 seconds
      Timer(Duration(seconds: 10), () {
        print('Synching time after 10 seconds');
        syncAppDeviceTime();
      });

      //AppMasterManager.instance().getInitData(_key, {'id': 'index'});
      print('INITIALIZING ESPOBJECT SUCCESSFUL!');
    }
  }

  void checkDeviceConnect(address) async {
    isServerUp = false;
    //print('Checking device conenction');
    if (isServerUp == false) {
      try {
        Socket espSocket;
        espSocket =
            await Socket.connect(address, 8080, timeout: Duration(seconds: 2));
        isServerUp = true;
        var c =
            AppMasterManager.instance().deviceESPHolder[_key].isSocketConnected;
        if (c == false) {
          AppMasterManager.instance().deviceESPHolder[_key].closeSocket();
          connectToDevice();
        } else {
          //
        }
        //print("Socket status : $c");
        if (espSocket != null) {
          //print('Server is up. Closing socket now.');
          espSocket.flush();
          espSocket.close();
          espSocket.destroy();
        }
      } catch (e) {
        isServerUp = false;
        //print('Cant connect to server, probably off or down');
      }
    } else {
      //print('Device is UP!');
    }
  }

  //#################################################################//
}
