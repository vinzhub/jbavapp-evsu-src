import 'dart:ui';
import 'counter_service.dart';
import 'background_main.dart';
import 'app_retain_widget.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
  
  //Channel serves as a bridge for exchanging datas from dart -> kotlin native
  var channel = const MethodChannel('com.example/background_service');
  var callbackHandle = PluginUtilities.getCallbackHandle(backgroundMain);
  channel.invokeMethod('startService', callbackHandle.toRawHandle());

  CounterService.instance().startCounting();
  //VinzService.instance().startCounting();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Background Demo',
      home: AppRetainWidget(
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Background Demo'),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            ValueListenableBuilder(
              valueListenable: CounterService.instance().count,
              builder: (context, count, child){
                return Text('Counting: $count');
              },
            ),
            // ValueListenableBuilder(
            //   valueListenable: VinzService.instance().count,
            //   builder: (context, count, child){
            //     return Text('Vinz Counter: $count');
            //   }
            // ,)
          ],
        ),
      ),
    );
  }
}
