import 'dart:io';

import 'package:flutter/foundation.dart';

import 'counter.dart';

class CounterService {
  factory CounterService.instance() => _instance;

  CounterService._internal();

  static final _instance = CounterService._internal();

  final _counter = Counter();

  ValueListenable<int> get count => _counter.count;

  void connectToServer() async {
    print('Trying to connect.........');
    Socket espSocket = await Socket.connect('192.168.8.101', 8080);
    espSocket.timeout(Duration(seconds: 30));
    
    if(espSocket != null){
      espSocket.flush();
      espSocket.close();
      espSocket.destroy();
      print("Socket closed. Socket closed. Socket closed. Socket closed. Socket closed.");
    }
  }

  void startCounting() {
    Stream.periodic(Duration(seconds: 8)).listen((_) {
      _counter.increment();
      print('Background incrementing :  ${_counter.count.value}');
      connectToServer();
    });
  }
}
