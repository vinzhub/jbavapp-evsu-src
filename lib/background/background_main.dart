import 'package:flutter/cupertino.dart';
import 'counter_service.dart';

void backgroundMain() {
  WidgetsFlutterBinding.ensureInitialized();
  print("BACKGROUND STARTING!");

  //IF UNCOMMENTED THE COUNTING CONTINUES AT THE BACKGROUND
  //CounterService.instance().startCounting();
  //COMMENT THIS OUT TO STOP BACKGROUND PROCESS AND START OVER AT APP START/INIT
}
