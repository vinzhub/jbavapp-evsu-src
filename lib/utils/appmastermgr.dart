import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'dart:ui';
import 'package:battery/battery.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jbavapp/udp/udp_test.dart';
import 'package:path_provider/path_provider.dart';
import 'chargingmode.dart';

//OUR MAIN APP CLASS MANAGER
//MANAGES ALL STATES AND STYLES
class AppMasterManager {
  //############################################################
  //SET CLASS AS SINGLETON
  factory AppMasterManager.instance() => _instance;
  AppMasterManager._internal();
  static final _instance = AppMasterManager._internal();
  //############################################################

  //############################################################
  //############################################################
  //APP MANAGER VARIALBES/STATES OF DEVICES
  //APP SETTINGS AND CONFIGS

  // ##############################################################
  //GLOBAL APP STYLES
  TextStyle title1 = TextStyle(
    fontSize: 55,
    fontWeight: FontWeight.bold,
    color: Colors.green,
  );

  TextStyle button1 = TextStyle(
    fontSize: 25,
  );
  // ##############################################################

  // Update Listeners call setState/Refresh on all listener
  // ##############################################################
  Map<String, Function> listeners = {};
  Timer updateTimer;

  void clearListeners() {
    listeners.clear();
  }

  void onUpdateEvent(type) {
    //for(var s in listeners.keys){
    if (type == 'relay') {
      //Update relays
      if (listeners.containsKey('relay')) {
        listeners['relay']();
        print('UPDATE EVENT ON RELAY');
      }
    } else if (type == 'timer') {
      //Update timers
      if (listeners.containsKey('timer')) {
        listeners['timer']();
        print('UPDATE EVENT ON TIMER');
      }
    } else if (type == 'sensor') {
      //Update sensor
      if (listeners.containsKey('sensor')) {
        listeners['sensor']();
        print('UPDATE EVENT ON SENSOR');
      }
    } else if (type == 'charging') {
      //update charge
      if (listeners.containsKey('charging')) {
        listeners['charging']();
        print('UPDATE EVENT ON CHARGING');
      }
    } else {
      //none
    }
    //}
  }
  // ##############################################################
  //

  //User of this app
  String appUser = "LMY48Z";

  //holds all devices found in the network broadcasting its name only
  List<String> networkDevices = [];
  //holds the device name and its address in a separate map
  Map<String, String> networkDevicesInfo = {};

  //when adding new device a name is ask and the name is keep here
  List<String> profiledDevices = [];
  //this holds the profiledDevice name and address
  Map<String, String> profiledDevicesInfo = {};

  //Holds the espobject of each devices in the network
  //this dictionary will be the one to hold all the espobjects
  //this will be used mostly
  Map<String, ESPObject> deviceESPHolder = {};

  //cache of current time
  TimeOfDay currentTime = TimeOfDay.fromDateTime(DateTime.now());

  //T0D0 : TIMER SHOULD UPDATE EVERY SECONDS BASED ON REMAINING TIME BEFORE IT ENDS
  // PROCESS HERE EVERY SECONDS TO UPDATE VALUES IN APP AND MATCH IT WITH DEVICE SIDE
  //Holds all timer from the current devices which are added by the user
  //List<String> deviceTimers = [];
  //Holds timer info like timer name, timer values[hour:minutes:seconds]
  //Map<String, dynamic> deviceTimersInfo = {};

  //Holds all schedules from the current devices which are added by the user
  //List<String> deviceSchedules = [];
  //Holds timer info like schedule name, schedules values[hour:minutes:seconds]
  //Map<String, dynamic> deviceSchedulesInfo = {};

  //Network device scanner
  bool isScanning = false;
  final String broadcastAddress = '224.0.2.200';
  final int port = 7777;
  UDPTest udpScanner = UDPTest();

  //Charging Mode Variables
  Battery battery = Battery();
  //BatteryState _batteryState;
  StreamSubscription<BatteryState> _batteryStateHandler;
  var channel = const MethodChannel('com.example/background_service');
  var callbackHandle = PluginUtilities.getCallbackHandle(backgroundMain);

  //
  DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  //Handle tab controlling on ProfileInnerPage
  int currentTab = 0;
  void setCurrentTab(tab) {
    currentTab = tab;
  }

  // void correctTab() {
  //   tabController.animateTo(currentTab, duration: Duration(seconds: 1));
  // }
  //############################################################
  //############################################################

  //############################################################
  //############################################################
  void checkNetworkDevice(n, a) {
    if (networkDevices.length > 0) {
      var isDuplicate = true;
      //Check the list of networks names
      for (var d in networkDevices) {
        if (n == d) {
          isDuplicate = true;
          //print('${networkDevicesInfo[d]} - $a');
          break;
        } else {
          isDuplicate = false;
        }
      }
      //now check for same name and address
      //if same name but different address
      //change the address
      for (var k in networkDevicesInfo.keys) {
        if (n == k && a != networkDevicesInfo[k]) {
          //print("Device name is the same but new Address");
          networkDevicesInfo[k] = a;
          //Now check the profiledDevices if it has already been added
          //and change the address there as well
          if (profiledDevices.contains(k) == true) {
            profiledDevicesInfo[k] = a;
          }
          break;
        }
      }

      if (isDuplicate == false) {
        addNetworkDevice(n, a);
      }
    } else {
      print('Adding new device from network.');
      addNetworkDevice(n, a);
    }
  }

  void initScanning() async {
    udpScanner.addReceiveEventListener(RESPONSE, (Datagram datagram) {
      var deviceName = ascii.decode(datagram.data);
      var deviceAddress = datagram.address.address;
      checkNetworkDevice(deviceName, deviceAddress);
    });
    await udpScanner.bindMulticastServer(broadcastAddress, port);
  }
  //############################################################
  //############################################################

  //############################################################
  //############################################################
  //Helper methods
  //############################################################
  //############################################################
  addNetworkDevice(name, address) {
    networkDevices.add(name);
    networkDevicesInfo[name] = address;
  }

  getProfiledDeviceAddress(name) {
    return profiledDevicesInfo[name];
  }

  addProfiledDevice(name, address) {
    profiledDevices.add(name);
    profiledDevicesInfo[name] = address;
  }

  removeProfiledDevice(name) {
    profiledDevices.remove(name);
    profiledDevicesInfo.remove(name);
    deviceESPHolder.remove(name);
    for (var i in deviceESPHolder.keys) {
      var v = deviceESPHolder[i];
      print('Present Devices : $i  VALUE : $v');
    }
  }

  addDeviceESP(key, device) {
    deviceESPHolder[key] = device;
    for (var i in deviceESPHolder.keys) {
      var v = deviceESPHolder[i];
      print('NEW DEVICES : $i  VALUE : $v');
    }
  }

  clearTimers() {
    //deviceTimers = [];
    //deviceTimersInfo = {};
  }

  clearSchedules() {
    //deviceSchedules = [];
    //deviceSchedulesInfo = {};
  }

  isInDevice(key) {
    if (deviceESPHolder.containsKey(key)) {
      return true;
    } else {
      return false;
    }
  }

  getDeviceAddress(key) {
    return deviceESPHolder[key].deviceAddress;
  }

  setActiveESPObject(key, state) {
    deviceESPHolder[key].isOwnerActive = state;
    //deviceESPHolder[key].isSocketConnected = true;
  }

  setEventHandler(key, Function func) {
    //deviceESPHolder[key].notificationHandler = func;

    //test to replace above method
    //set Appmastermgr updatefunction
    deviceESPHolder[key].notificationHandler = onUpdateEvent;
    // print('==========================================================');
    // print('==========================================================');
    // print('NEW UPDATE EVENT CALLED');
    // print('==========================================================');
    // print('==========================================================');
  }

  activateESPObject(key) {
    try {
      deviceESPHolder[key].initSocket();
      deviceESPHolder[key].espKey = key;
    } catch (e) {}
  }

  getInitData(key, data) {
    deviceESPHolder[key].outGoingData(data);
    print('INIT DATA REQUEST SENT!');
  }

  getRelayState(relay, key) {
    if (relay == 'A') {
      return deviceESPHolder[key].relayAState;
    } else if (relay == 'B') {
      return deviceESPHolder[key].relayBState;
    }
  }

  getSensorState(key, port) {
    if (port == 1) {
      return deviceESPHolder[key].sensorAState;
    } else {
      return deviceESPHolder[key].sensorBState;
    }
  }

  setSensorState(key, port, state) {
    if (port == 1) {
      deviceESPHolder[key].sensorAState = state;
      //send a delete/remove data to ESPDEVICE
      var data = {'id': 'remsensor', 'port': port};
      sendDataESPDevice(key, data);
      onUpdateEvent('sensor');
    } else {
      deviceESPHolder[key].sensorBState = state;
      //send a delete/remove data to ESPDEVICE
      var data = {'id': 'remsensor', 'port': port};
      sendDataESPDevice(key, data);
      onUpdateEvent('sensor');
    }
  }

  getSensorValues(key, port) {
    if (port == 1) {
      return '${deviceESPHolder[key].sensorAName} - ${deviceESPHolder[key].sensorAValue}';
    } else {
      return '${deviceESPHolder[key].sensorBName} - ${deviceESPHolder[key].sensorBValue}';
    }
  }

  //Test method for adding new timers
  addNewTimer(key) {
    //var l = deviceESPHolder[key].deviceTimers.length;
    //deviceESPHolder[key].deviceTimers.add((l + 1).toString());
  }

  addNewSched(key) {
    //var l = deviceESPHolder[key].deviceSchedules.length;
    //deviceESPHolder[key].deviceSchedules.add((l + 1).toString());
  }

  sendDataESPDevice(key, data) {
    deviceESPHolder[key].outGoingData(data);
  }

  setChargingMode(state) {
    if (state == true) {
      _batteryStateHandler =
          battery.onBatteryStateChanged.listen((BatteryState state) {
        //setState(() {
        //_batteryState = state;
      });
    } else {
      _batteryStateHandler.cancel();
    }
  }

  getChargeMode(key) {
    return deviceESPHolder[key].inChargingMode;
  }

  setChargeMode(key, String chargePoint, int relay) {
    print("SETTING $key - CHARGE POINT IS $chargePoint - RELAY IS $relay");
    print("ESPADDRESS : ${deviceESPHolder[key].deviceAddress}");
    deviceESPHolder[key].onChargeMode(relay, int.parse(chargePoint));
  }

  clearChargeMode(key) {
    deviceESPHolder[key].inChargingMode = false;
    deviceESPHolder[key].chargingPoint = 0;
    deviceESPHolder[key].chmodeRelay = 0;
  }
  // int getBatteryPercentage() async{
  //   final int b = await _battery.batteryLevel;
  //   print("BATTERY : $b");
  //   return b;
  // }

  //deviceESPHolder
  //############################################################
  //HELPER WIDGETS
  Future<TimeOfDay> showTimePicker2(BuildContext context) async {
    final picked = await showTimePicker(
      context: context,
      initialTime: currentTime,
    );
    return picked;
  }

  Future<String> createTimerDialog(BuildContext context) {
    TextEditingController _relay = TextEditingController();
    //TextEditingController _controller = TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('NEW TIMER'),
            //content: ,
            content: Column(
              children: <Widget>[
                Text('Period'),
                TextField(
                  controller: _relay,
                ),
                Text('Hour'),
                TextField(
                  controller: _relay,
                ),
                Text('Minute'),
                TextField(
                  controller: _relay,
                ),
              ],
            ),
            actions: [
              MaterialButton(
                elevation: 5.0,
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  //DIALOG FOR SETTING A SENSOR
  //GIVING A CHOICE IF ITS AN ANALOG OR A DIGITAL SENSOR
  Future<String> createSensorType(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('SENSOR TYPE:'),
            ],
          ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: OutlineButton(
                  onPressed: () {
                    //print('ANALOG SENSOR');
                    Navigator.of(context).pop('ag');
                  },
                  child: Text('ANALOG'),
                ),
              ),
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: OutlineButton(
                  onPressed: () {
                    //print('DIGITAL SENSOR');
                    Navigator.of(context).pop('dg');
                  },
                  child: Text('DIGITAL'),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  //For adding new Profile in the HomePage
  Future<String> createDialog(BuildContext context) {
    TextEditingController _controller = TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Enter Profile Name:'),
            content: TextField(
              controller: _controller,
            ),
            actions: [
              MaterialButton(
                elevation: 5.0,
                child: Text('Add'),
                onPressed: () {
                  Navigator.of(context).pop(_controller.text.toString());
                },
              ),
            ],
          );
        });
  }

  //For confirmation Yes or No
  Future<String> createYesOrNo(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Are you SURE!?'),
          actions: [
            MaterialButton(
              elevation: 5.0,
              child: Text('YES'),
              onPressed: () {
                Navigator.of(context).pop('YES');
              },
            ),
            MaterialButton(
              elevation: 5.0,
              child: Text('NO'),
              onPressed: () {
                Navigator.of(context).pop('NO');
              },
            ),
          ],
        );
      },
    );
  }

  //############################################################

  //############################################################
  //UTILITIES
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/config.txt');
  }

  Future<String> readConfig() async {
    try {
      final file = await _localFile;
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      return 'empty';
    }
  }

  Future<File> writeConfig(String contents) async {
    final file = await _localFile;
    return file.writeAsString(contents);
  }

  Future<File> _saveConfig(contents) {
    writeConfig(contents);
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;
    deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
    print('DEVICE INFO : $deviceData');
    //Set AppManager appuser to this
    appUser = deviceData['id'];
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'id': build.id,
      'model': build.model,
    };
  }

  // saveConfigVer2() async {
  //   //get needed config for save
  //   List<String> netScan1 = networkDevices;
  //   Map<String, String> netScan2 = networkDevicesInfo;

  //   List<String> netProf1 = profiledDevices;
  //   Map<String, String> netProf2 = profiledDevicesInfo;
  // }

  saveConfig() async {
    var forSave = {};
    //Get list of scanned devices found in network
    //and list of profiled devices
    // #1
    var netSave = {};
    for (var f in networkDevicesInfo.keys) {
      netSave[f] = networkDevicesInfo[f];
    }

    //Add #1 to forSave Map
    //forSave['networkscan'] = netSave;

    //String encodedNetSave = jsonEncode(netSave);
    // #2
    var profSave = {};
    for (var f in profiledDevicesInfo.keys) {
      //profSave.add([f, profiledDevicesInfo[f]]);
      profSave[f] = profiledDevicesInfo[f];
    }
    //Add #2 to forSave Map
    forSave['profiledevice'] = profSave;
    //Now encode this Map to json
    String encodedConfig = jsonEncode(forSave);

    // #3
    // OPEN FILE/WRITE encoded strings
    // SAVE/CLOSE FILE
    // EXIT/INIT APP
    _saveConfig(encodedConfig);
    print('WRITING DONE');
  }

  loadConfig() async {
    try {
      var config = await readConfig();
      var dc = json.decode(config);

      if (dc != null) {
        var ns = dc['networkscan'];
        var pd = dc['profiledevice'];
        //#1 load network scanned device and add to AppManager
        // for (var keys in ns.keys) {
        //   print('NETWORK DEVICE : $keys - ${ns[keys]}');
        //   //INITIALIZED NETWORK SCAN LIST
        //   checkNetworkDevice(keys, ns[keys]);
        // }
        for (var keys in pd.keys) {
          print('PROFILED DEVICE : $keys - ${pd[keys]}');
          //INITIALIZED PROFILED DEVICE
          addProfiledDevice(keys, pd[keys]);
        }
      }
    } catch (e) {}
  }
  //############################################################
}

class ESPObject {
  //#########################################
  var espKey = '';
  //#########################################
  /* DEVICE ATTRIBUTES AND INFOS*/
  var devicePORT = 80;
  var deviceName = '';
  var deviceAddress = '';
  var relayAState = '';
  var relayBState = '';
  var sensorAState = false;
  var sensorBState = false;
  var sensorAName = '';
  var sensorBName = '';
  var sensorAValue = '';
  var sensorBValue = '';

  //charging mode
  bool inChargingMode = false;
  int chargingPoint = 0;
  int chmodeRelay = 0;

  List<String> deviceTimers = [];
  Map<String, dynamic> deviceTimersValues = {};
  List<String> deviceSchedules = [];
  Map<String, dynamic> deviceSchedulesValues = {};
  var deviceConfig = '';

  ESPObject(this.deviceName, this.deviceAddress);
  //#########################################
  //#########################################

  //#########################################
  //#########################################
  /* DEVICE Network Object and Atrributes */
  Socket deviceSocket;
  bool isSocketConnected = false;
  bool isOwnerActive = false;
  Function notificationHandler;
  //#########################################
  //#########################################

  //#########################################
  //#########################################
  /* DATA/PACKET HANDLERS */
  //Test modified handler
  TimeOfDay currentTime = TimeOfDay.fromDateTime(DateTime.now());
  DateTime dt;
  void onDataHandler(List data) {
    dt = DateTime.now();
    var d = utf8.decode(data);
    List sd = d.split('>>');
    sd.removeAt(sd.length - 1);
    for (var s in sd) {
      var jsonDecodedData = jsonDecode(s);
      //print('Data received - ${dt.toUtc()}  \nDATA IS : $jsonDecodedData');
      inGoingData(jsonDecodedData);
    }
  }

  //THROWS ERROR WHEN MULTIPLE SERIALIZED JSON IS ADDED IN THE STREAM
  //USE the new onDataHandler
  void onDataHandler_Original(List data) {
    if (data.isNotEmpty) {
      var d = utf8.decode(data);
      //check for a server closed packet first
      if (d == 'serverclosed') {
        closeSocket();
      } else {
        //data is not serverclose now process it in another handler
        var jsonDecodedData = jsonDecode(d);
        print('Decoded data is : $jsonDecodedData');
        //print(jsonDecodedData);
        inGoingData(jsonDecodedData);
      }
    }
  }

  void onErrorHandler(error) {
    print('An error occured : $error');
  }

  void onDoneHandler() {}
  //#########################################
  //#########################################

  //#########################################
  //#########################################
  /* NETWORK/SOCKET PARTICULARS */
  //Init socket and listeners
  void initSocket() async {
    if (isSocketConnected == false) {
      print('Initializing socket...');
      try {
        deviceSocket = await Socket.connect(deviceAddress, devicePORT);
        deviceSocket.timeout(Duration(seconds: 15));
        print('Socket connected to device.');
        isSocketConnected = true;
        //Initialize request - device info and states
        outGoingData({'id': 'index'});
        print('Init packet sent!');

        //Set socket event handlers
        deviceSocket.listen(onDataHandler,
            onError: onErrorHandler, onDone: onDoneHandler);
      } catch (e) {
        print('ERROR : ${e.toString()}');
        print('An error occured while initilizing the socket.');
        isSocketConnected = false;
      }
    } else {
      print('Socket is already initialized.');
    }
  }

  //flush and destroy a socket object
  void closeSocket() {
    print('Socket is closing.');
    if (isSocketConnected == true) {
      deviceSocket.flush();
      deviceSocket.close();
      deviceSocket.destroy();
      isSocketConnected = false;
    }
  }

  //handle network packets received from the device
  void inGoingData(data) {
    //var result = '';
    if (data['sensor_read'] == true) {
      onSensorUpdate(data);
    } else if (data['id'] == null) {
      return;
    } else {
      if (data['id'] == 'index') {
        //Device info and states
        //result = 'INIT DATA';
        //#1 Update relays
        //#2 Update timers and scheds
        //#3 Update sensors
        print('Setting Relay');
        onRelayUpdate(data);
        print('Setting timer/schedule');
        onTimerSchedUpdate(data);
        print('Setting sensors');
        onSensorUpdate(data);
        print('Setting charging mode');
        setChargeMode(data);
        //Update all widgets
        notificationHandler('relay');
        notificationHandler('timer');
        notificationHandler('sensor');
        notificationHandler('charging');
      } else if (data['id'] == 'pins') {
        //result = 'PINS UPDATED';
        //print('IN PINS');
        //print(data);
        onRelayUpdate(data);

        //test new notification
        notificationHandler('relay');
      } else if (data['id'] == 'ADDTIMERSCHED_OK') {
        //result = 'NEW TIMERADDED';
        onTimerSchedUpdate(data);
        notificationHandler('timer');
      } else if (data['id'] == 'ADDSENSOR_OK') {
        //result = 'NEW SENSOR SET';
        onSensorUpdate(data);
      } else if (data['id'] == 'gettimer') {
        //clean timer
        //on new timer arriveq
        print('TIMERS FETCH are : $data');
        onTimerSchedUpdate(data);
        notificationHandler('timer');
      } else if (data['id'] == 'addtimer') {
        print('A NEW TIMER HAS BEEN SET.');
        //REQUEST A FULL LIST OF TIMERS
        var d = {'id': 'gettimers'};
        outGoingData(d);
        //{id: addtimer, data: r1off - [21, 40, False, 0, 0, 'r1', False]}
      }

      print('INBOUND DATA OCCURED');
      // if (notificationHandler != null) {
      //   notificationHandler();
      //   //test
      // }
    }
  }

  //handler network packets to be sent to device
  //Data should be in a dictionary type already for json encode ready.
  void outGoingData(data) {
    if (isSocketConnected == false) {
      return;
    } else {
      print(data);
      var encodedData = jsonEncode(data);
      //Add a end of line expression at the end of the byte/data to tell the server to stop reading.
      encodedData = encodedData + '\n';
      deviceSocket.add(utf8.encode(encodedData));
      //Clean tcp socket buffer? maybe heheh
      deviceSocket.flush();
      print('OUT GOING DATA FOUND! $encodedData');
    }
  }
  //#########################################
  //#########################################

  //#########################################
  //#########################################
  /* HELPER METHODS */
  //calls when adding a new timer/schedule to device
  void onAddTimerSched(data) {}

  //calls when removing a certain timer/schedule
  void onDeleteTimerSched(data) {}

  //calls when a new updated timer/sched is received from the device
  void onTimerSchedUpdate(data) {
    //{id: addtimer, data: r1off - [21, 40, False, 0, 0, 'r1', False]}
    //deviceTimers
    //deviceTimersValues
    if (data['data'] == null) {
      print('Data not a timer.');
      return;
    }
    deviceTimers.clear();
    deviceTimersValues.clear();
    var timerValues = data['data'];
    for (var k in timerValues.keys) {
      var value = timerValues[k];
      var relay = value[5];
      var state = value[6];

      //print('TIMER VALUES : $timerValues');
      //Add to espobject timers/timersvalues
      deviceTimers.add(k);
      var h = value[0];
      var m = value[1];
      print('TIME IS - $h : $m');
      deviceTimersValues[k] = value;

      if (relay == 'r1') {
        print('RELAY A');
        if (state == true) {
          relayAState = '1';
        } else {
          relayAState = '0';
        }
      } else if (relay == 'r2') {
        print('RELAY B');
        if (state == true) {
          relayBState = '1';
        } else {
          relayBState = '0';
        }
      }
    }
    print('RELAY UPDATED VIA NEW TIMERS');
  }

  //calls when setting relay state and sending it to device
  void onSetRelay(data) {}

  //calls when a new relay state is received from the device
  void onRelayUpdate(data) {
    //var v = data['state'];
    //print('Values are : $v');

    //fetch state values/infos
    var relayA = data['state'][0][1];
    var relayB = data['state'][1][1];
    relayAState = relayA.toString();
    relayBState = relayB.toString();
  }

  //Activate ChargingMode
  void onChargeMode(relay, point) {
    chmodeRelay = relay;
    chargingPoint = point;
    //inChargingMode = true;
  }

  void setChargeMode(data) {
    var user = data['user'];
    //Determine if the user who activated charge-mode is the same as this user
    if (user == AppMasterManager._instance.appUser) {
      var c = data['chargemode'];
      var cp = data['cpoint'];
      print("ON INDEX CPOINT IS : $cp");
      if (c == true) {
        inChargingMode = true;
        //Next is start the background-service
        AppMasterManager.instance().channel.invokeMethod('startService',
            AppMasterManager.instance().callbackHandle.toRawHandle());
        //ChargingModeService.instance().deviceName = 'test device';
        //ChargingModeService.instance().deviceAddress = '192.168.254.102';
        ChargingModeService.instance()
            .setAddress(AppMasterManager.instance().getDeviceAddress(espKey));
        ChargingModeService.instance().setChargingPoint(cp);
        ChargingModeService.instance().chargingMonitor();
      } else if (c == false) {
        inChargingMode = false;
      }
    } else {
      //You are not the user who activated the Charge-mode
      //ignore setting charge-mode background service
    }
  }

  //calls when setting a new sensor and sending it to device
  void onSetSensor(data) {}

  //calls when a new sensor value/state is received from the device
  void onSensorUpdate(data) {
    //Check sensor name to specified how values will be set
    var values = data['values'];
    //print(values);
    var sAName = values[0];
    var sAValue = values[1];
    sensorAName = sAName.toString();
    sensorAValue = sAValue.toString();
    sensorAState = true;

    var sBName = values[2];
    var sBValue = values[3];
    sensorBName = sBName.toString();
    sensorBValue = sBValue.toString();
    sensorBState = true;

    print('$sensorAName - $sensorAValue  |  $sensorBName : $sensorBValue');


    // //print('SENSOR UPDATED');
    // var values = data['values'];
    // if (values == null) {
    //   return;
    // }
    // //set sensor A
    // var s1 = values[0];
    // var s1v = values[1];
    // // //set sensor B
    // var s2 = values[2];
    // var s2v = values[3];
    // print('FIRST PART DONE');

    // // print('$s1 $s1v $s2 $s2v');

    // if (s1 != 0) {
    //   sensorAName = s1;
    //   print('Sensor A NAME DONE');
    //   sensorAValue = s1v;
    //   print('Sensor A VALUE DONE');
    //   sensorAState = true;
    //   print('Sensor A STATE DONE');
    // }
    // if (s2 != 0) {
    //   sensorBName = s2;
    //   sensorBValue = s2v;
    //   sensorBState = true;
    // }

    if (notificationHandler != null) {
      //notificationHandler();
      notificationHandler('sensor');
    }
  }

  //called when APP is closing and saving all Objects state in a json file
  void onObjectSerialize() {}

  //called when APP is initialized and found a saved config at storage and load and deserialize all states
  void onObjectDesialize() {}
  //#########################################
  //#########################################

}

//IMPLEMENT BUAS :)
class ConfigObject {
  final List<String> scan1;
  final Map<String, String> scan2;
  final List<String> profile1;
  final Map<String, String> profile2;

  ConfigObject(this.scan1, this.scan2, this.profile1, this.profile2);

  ConfigObject.fromJson(Map<String, dynamic> json)
      : scan1 = json['scan1'],
        profile1 = json['profile1'],
        scan2 = json['scan2'],
        profile2 = json['profile2'];

  Map<String, dynamic> toJson() => {
        'scan1': scan1,
        'profile1': profile1,
        'scan2': scan2,
        'profile2': profile2
      };
}
