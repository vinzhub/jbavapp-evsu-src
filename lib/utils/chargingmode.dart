import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';

import 'appmastermgr.dart';

class ChargingMode {
  ChargingMode() {
    _readAddress().then((address) => _address.value = address);
    _readCP().then((cp) => _cp.value = cp);

    print("INIT ADDRESS : $currentAddress  -  CP : $chargingPoint");
  }

  ValueNotifier<String> _address = ValueNotifier('');
  ValueListenable<String> get address => _address;
  ValueNotifier<int> _cp = ValueNotifier(0);
  ValueListenable<int> get cp => _cp;

  Socket deviceSocket;
  String currentAddress = '';
  String currentRelay = ''; //r1 or r2
  int chargingPoint = 50;
  int sendTimes = 0;

  void process() {
    checkBattery();
    _writeAddress(currentAddress);
    _writeCP(chargingPoint);
  }

  checkBattery() async {
    final int level = await AppMasterManager.instance().battery.batteryLevel;
    print("BATTERY AT : $level");
    if (level >= chargingPoint) {
      //battery is almost full
      //add a couple of seconds or minutes to make sure for
      //battery percentage tolerance

      //prepare data to be sent
      var data = {'id': 'switch', 'rs': 'r1', 'state': 1};
      var encodedData = jsonEncode(data);
      encodedData = encodedData + '\n';

      print("BATTERY POINT!");

      //check if socket is initilized
      if (deviceSocket == null) {
        String addr = await _readAddress();
        if (addr != null) {
          deviceSocket = await Socket.connect(addr, 80);
          deviceSocket.timeout(Duration(seconds: 30));
          deviceSocket.listen(onDataHandler);
        }
      } else {
        deviceSocket.add(utf8.encode(encodedData));
        deviceSocket.flush();
      }
    }
  }

  void onDataHandler(List data) {
    if (data.isNotEmpty) {
      var d = utf8.decode(data);
      //check for a server closed packet first
      if (d == 'serverclosed') {
        print("SERVER JUST CLOSED GOT FROM CHARGINGMODE");
      } else {
        // //data is not serverclose now process it in another handler
        // var jsonDecodedData = jsonDecode(d);
        // print('Decoded data is : $jsonDecodedData READ FROM CHARGINGMODE');
        // //get id : pins only
        // //to determine if the pin we used as chargeMode is already off
        // if (jsonDecodedData['id'] == 'pins') {}
      }
    }
  }

  Future<String> _readAddress() async {
    var prefs = await SharedPreferences.getInstance();
    print("Reading address from Shared Preference");
    String a = prefs.getString('address') ?? '';
    if (a != '') {
      currentAddress = a;
    }
    return a;
  }

  Future<int> _readCP() async {
    var prefs = await SharedPreferences.getInstance();
    print("Reading address from Shared Preference");
    int a = prefs.getInt('cp') ?? 0;
    if (a != 0) {
      chargingPoint = a;
    }
    return a;
  }

  Future<void> _writeAddress(String address) async {
    var prefs = await SharedPreferences.getInstance();
    print("Writing address : $address to Shared Preference");
    return prefs.setString('address', address);
  }

  Future<void> _writeCP(int cp) async {
    var prefs = await SharedPreferences.getInstance();
    print("Writing CP : $cp to Shared Preference");
    return prefs.setInt('cp', cp);
  }
}

class ChargingModeService {
  factory ChargingModeService.instance() => _instance;
  ChargingModeService._internal();
  static final _instance = ChargingModeService._internal();
  final _chargingMode = ChargingMode();

  //Change name later
  final _backChannel = const MethodChannel('com.example/background_service');
  Timer batteryMonitor;
  bool batteryMonitorOn = false;
  var batteryValue;
  int batteryLevel = 0;

  //ESPDEVICE INFO
  var deviceName = "";
  var deviceAddress = "";

  Socket espSocket;
  Stream stream;
  //StreamSubscription streamCall;

  void setAddress(addr) {
    print("Setting addres of chargingClass : $addr");
    _chargingMode.currentAddress = addr;
  }

  void setChargingPoint(cp) {
    print("SETTING CHARGING POINT : $cp");
    _chargingMode.chargingPoint = cp;
  }

  void chargingMonitor() {
    if (batteryMonitorOn == false) {
      print("MONITORING STARTED!");
      batteryMonitor = Timer.periodic(
        Duration(seconds: 5),
        (batteryMonitor) {
          _chargingMode.process();
        },
      );
      batteryMonitorOn = true;
    }
  }

  void stopMonitor() {
    //if (batteryMonitorOn == true) {
    _backChannel.invokeMethod('stopService');
    if (batteryMonitor != null) {
      batteryMonitor.cancel();
    }
    batteryMonitorOn = false;
    print("MONITORING STOPPED!");
    //}
  }
}

void backgroundMain() {
  WidgetsFlutterBinding.ensureInitialized();
  print("BACKGROUND STARTING!");

  //IF UNCOMMENTED THE COUNTING CONTINUES AT THE BACKGROUND
  //CounterService.instance().startCounting();
  //ChargingModeS ervice.instance().deviceName = 'test device';
  //var add = ChargingModeService.instance().deviceAddress;
  //print("ADDRESS IS - $add");
  //ChargingModeService.instance().deviceAddress = add;
  ChargingModeService.instance().chargingMonitor();
  //COMMENT THIS OUT TO STOP BACKGROUND PROCESS AND START OVER AT APP START/INIT
}

// void connectToServer() async {
//   print('Trying to connect......... : $deviceAddress');
//   Socket espSocket = await Socket.connect('192.168.254.102', 8080);
//   espSocket.timeout(Duration(seconds: 30));

//   if (espSocket != null) {
//     espSocket.flush();
//     espSocket.close();
//     espSocket.destroy();
//     print("Socket closed.");
//   }
// }

// getBatteryLevel() async {
//   //USE BATTERY PACKAGE
//   final int level = await AppMasterManager.instance().battery.batteryLevel;
//   batteryLevel = level;
//   print("BATTERY Level : $batteryLevel %");

//   if (level > 45) {
//     //set data to be sent
//     var data = {'id': 'switch', 'rs': 'r1', 'state': 1};
//     var encodedData = jsonEncode(data);
//     encodedData = encodedData + '\n';
//     if (espSocket == null) {
//       espSocket = await Socket.connect('192.168.254.102', 80);
//       espSocket.timeout(Duration(seconds: 30));
//     }

//     if (espSocket != null) {
//       espSocket.add(utf8.encode(encodedData));
//       espSocket.flush();
//       print("Socket closed.");
//     }
//   } else {
//     connectToServer();
//   }

//   //print("SENDING PACKET TO : $deviceName WITH ADDRESS : $deviceAddress");

//   //ANDROID KOTLIN BATTERY FETCH
//   // var res = await _channel.invokeMethod('testService');
//   // if (res != null) {
//   //   batteryValue = res;
//   //   print("BATTERY Level : $batteryValue %");
//   // }
//   //
// }

// testMonitor() {
//   print("TEST MONITOR");
// }

// void monitorCharging3() {
//   batteryMonitorOn = true;
//   print("Starting stream");
//   stream = Stream.periodic(Duration(seconds: 5), testMonitor());
//   // Stream.periodic(Duration(seconds: 8)).listen((streamCall) {
//   //   print("Streaming....");
//   //   connectToServer();
//   // });
// }

// void monitorCharging2() {
//   if (batteryMonitorOn == false) {
//     print("MONITORING STARTED!");
//     batteryMonitor = Timer.periodic(
//       Duration(seconds: 5),
//       (batteryMonitor) {
//         getBatteryLevel();
//         //connectToServer();
//       },
//     );
//     batteryMonitorOn = true;
//   }
// }
