import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jbavapp/page/Configpage.dart';
//import 'package:jbavapp/main/topheader.dart';
import 'package:jbavapp/page/homepage.dart';
import 'package:jbavapp/page/scandevicespage.dart';
import 'package:jbavapp/utils/appmastermgr.dart';
import 'package:jbavapp/utils/chargingmode.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  //int _index = 0;
  final _channel = const MethodChannel('com.example/app_retain');
  var channel = const MethodChannel('com.example/background_service');
  var callbackHandle = PluginUtilities.getCallbackHandle(backgroundMain);
  Timer batteryScan;
  bool timerOn = false;
  //TAB TYPE
  TabController tabController;
  String batteryP = "";
  TextEditingController _address = TextEditingController();

  @override
  void initState() {
    super.initState();
    //Load config files
    //AppMasterManager:
    //  - scannedDevices/Infos
    //  - loadedDevices
    AppMasterManager.instance().loadConfig();
    //At the same time load device id/model
    AppMasterManager.instance().initPlatformState();
    print("MAIN DART LOADING!");
  }

  @override
  void dispose() {
    super.dispose();
    AppMasterManager.instance().saveConfig();
    print("MAIN DART CLOSING!");
    Navigator.of(context).pop(true);
  }

  void addTestDevice() {
    AppMasterManager.instance()
        .checkNetworkDevice('TestESP', '192.168.254.102');
  }

  Widget testService() {
    return Column(
      children: [
        Text("Test MethodChannel"),
        // TextField(
        //   controller: _address,
        // ),
        OutlineButton(
          onPressed: () {
            channel.invokeMethod('startService', callbackHandle.toRawHandle());
            //ChargingModeService.instance().deviceName = 'test device';
            //ChargingModeService.instance().deviceAddress = '192.168.254.102';
            //ChargingModeService.instance().setAddress(_address.text);
            ChargingModeService.instance().setAddress('192.168.254.102');
            ChargingModeService.instance().setChargingPoint(50);
            ChargingModeService.instance().chargingMonitor();
          },
          child: Text("START MONITOR"),
        ),
        OutlineButton(
          onPressed: () {
            ChargingModeService.instance().stopMonitor();
          },
          child: Text("STOP TIMER"),
        ),
        Text("BATTERY : $batteryP"),
      ],
    );
  }

  List<Widget> homePages = <Widget>[
    HomePage(),
    ScanDevicesPage(),
    ConfigPage(),
  ];

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Exit Jbav Manager App?'),
            content: new Text('Are you sure?'),
            actions: <Widget>[
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: Text("NO"),
              ),
              SizedBox(height: 16),
              new GestureDetector(
                onTap: () {
                  _channel.invokeMethod(
                      'sendToBackground', {'text': 'Hello world'});
                  print("CLOSING FLUTTER");
                  dispose();
                  //Navigator.of(context).pop(true);
                },
                child: Text("YES"),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    tabController = new TabController(length: 3, vsync: this);
    var tabBarItem = new TabBar(
      tabs: [
        new Tab(
          icon: new Icon(Icons.home),
        ),
        new Tab(
          icon: new Icon(Icons.devices),
        ),
        new Tab(
          icon: new Icon(Icons.settings_applications),
        ),
      ],
      controller: tabController,
      indicatorColor: Colors.white,
    );
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: new DefaultTabController(
          length: 3,
          child: new Scaffold(
            appBar: new AppBar(
              centerTitle: true,
              //backgroundColor: Colors.blueAccent,
              //bottomOpacity: 0.5,
              actions: [
                OutlineButton(
                  onPressed: () {
                    print('Test Add Button added');
                    addTestDevice();
                  },
                  child: Text('Test Add'),
                ),
              ],
              title: Text(
                "JBAV Device Manager",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              bottom: tabBarItem,
            ),
            body: new TabBarView(
              controller: tabController,
              children: homePages,
              // children: [
              //   testService(),
              //   Text('1'),
              //   Text('2'),
              // ],
            ),
          ),
        ));
  }
}
