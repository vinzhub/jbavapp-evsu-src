package com.example.jbavapp
import android.util.Log
import android.os.Bundle
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity : FlutterActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Notifications.createNotificationChannels(this)
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        val binaryMessenger = flutterEngine.dartExecutor.binaryMessenger

        MethodChannel(binaryMessenger, "com.example/background_service").apply {
            setMethodCallHandler { method, result ->
                if (method.method == "startService") {
                    //Log.i("BackgroundService", "Flutter service started in App")
                    val callbackRawHandle = method.arguments as Long
                    BackgroundService.startService(this@MainActivity, callbackRawHandle)
                    result.success(null)
                } else if(method.method == "stopService") {
                    BackgroundService.stopService(this@MainActivity)
                    result.success(null)
                } else {
                    result.notImplemented()
                }
            }
        }

        MethodChannel(binaryMessenger, "com.example/app_retain").apply {
            setMethodCallHandler { method, result ->
                if (method.method == "sendToBackground") {
                    //Log.i("BACKGROUND SERVICE CREATED", "Flutter service started in App");
                    val text = method.argument<String>("text")
                    Log.i("FLUTTER SEND TO BACK ARG", text);
                    //showToast(text)
                    moveTaskToBack(true)
                    result.success(null)
                } else if(method.method == "testService"){
                    val batteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
                        context.registerReceiver(null, ifilter)
                    }

                    val batteryPct: Float? = batteryStatus?.let { intent ->
                        val level: Int = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
                        val scale: Int = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
                        level * 100 / scale.toFloat()
                    }                    
                    //Log.i("LEVEL",batteryPct.toString())
                    Log.i("TEST SERVICE", "Test service app started");
                    result.success(batteryPct)
                }
                else {
                    result.notImplemented()
                }
            }
        }
    }
}
