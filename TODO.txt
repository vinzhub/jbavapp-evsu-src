#RANDOM CRASH ON RELEASE
#WHEN SENDING SOCKET DATA
#WHEN ADDING TIMERS
#WHEN SCROLLING FOR TIMER IN REPEAT TIMER

#1. Implement ESPDevice UI - Add TCPSocket per Device - add send/recv command via async
#   1 TCP Socket for Sending and for Receiving.
#2. React on ESPDevice response. Re color UI From Red-Green  ON/OFF
#3. Implement ESPDevice save/load scanned/loaded device into phone directory via .json file.
#4. Implement adding of Timer into ESPDevice.
#5. 



######################################################################
######################################################################
#JSON Configs
REMOVE-TIMER : {"id":"deltimer","rs":"[RELAY NAME (r1 or r2)]"}
REFRESH-TIMER : {"id":"gettimers"}
SWITCH-ON : {"id":"switch", "rs":"[RELAY NAME (r1 or r2)]", "state":0}
SWITCH-OFF : {"id":"switch", "rs":"[RELAY NAME (r1 or r2)]", "state":1}
SYNC-TIME: {"id":"synctime", "year":year, "month":month, "day":day, "hour":hour, "minute":minute, "second":second}
SYNC-STATES: {"id": "index"}
SYNC-ALIVE: {"id":"alive"}
GET-RTC: {"id":"rtc"}

###########################################################
#timers
	var hour = int(get_node(HOUR).value)
	var minute = int(get_node(MINUTE).value)
	var period = get_node(PERIOD).text
	#handle 24-hour format
	if period == "PM":
		if hour < 12:
			hour = hour + 12
		elif hour == 12:
			hour = hour - 12
			period = "AM"
	print("HOUR:", hour)
	print("MINUTE:",minute)
	print("PERIOD:",period)
				
	#options
	var repeat = get_node(REPEAT).pressed
	if repeat == true:
		repeat = 1
	else:
		repeat = 0
	var state = get_node(STATE).pressed
	var rhour = int(get_node(RHOUR).value)
	var rmin = int(get_node(RMINUTE).value)
	
	print("REPEAT:",repeat)
	print("STATE:",state)
	print("RHOUR:",rhour)
	print("RMIN:",rmin)
	
	var s = 0
	var ss = ""
	if state == true:
		s = 0
		ss = "on"
	else:
		s = 1
		ss = "off"
	var r = relay + ss
###########################################################
SET-TIMER: {"id":"settimer", "rs":r, "value": [hour,minute, repeat, int(rhour), int(rmin), relay, state]}